echo "preparing moqui test environment"

COMP_FILE="${1:-moqui-pg-compose.yml}"
docker-compose -f $COMP_FILE -p moqui up -d